package amazon;

import java.util.Map;

public class Order {

    private int orderId;
    private Map<Item, Integer> itemCount;  // doubt map<Integer,item>

    public Map<Item, Integer> getItemCount() {
        return itemCount;
    }

    public void setItemCount(Map<Item, Integer> itemCount) {
        this.itemCount = itemCount;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }


}
