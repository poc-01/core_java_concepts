package concepts.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StreamExample1 {
    public static void main(String[] args) {
        List<String> mylist = Arrays.asList("a1", "a2", "b2", "b1", "b3","c1", "c2");

        mylist.stream().filter(s -> s.startsWith("b")).map(String::toUpperCase).sorted().forEach(System.out :: println);
System.out.println("find first");
mylist.stream().findFirst().ifPresent(System.out::println);
    }
}