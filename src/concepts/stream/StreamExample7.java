package concepts.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample7 {
    public static void main(String[] args) {
        Stream.of("a1","b4","t4","v4","y6","r5")
                .filter(s -> {
                    System.out.println("filter  " +s);
                    return true;
                })
               // .forEach(System.out::println);
        .forEach(s->{
            System.out.println("foreach "+s);
        });

    }
}