package concepts.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.*;

public class StreamExample4 {
    public static void main(String[] args) {
        List<String> mylist = new ArrayList<String>();
        mylist.add("a2");
        mylist.add("a4");
        mylist.add("a3");

        System.out.println("Array initial elements" + mylist);
        System.out.println("Array after 1st modification");
        mylist.stream().map(v->v.substring(1)).mapToInt(Integer::parseInt).max().ifPresent(System.out::println);



    }
}
