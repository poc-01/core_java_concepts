package concepts.stream.collectors;

import java.util.Arrays;
import java.util.List;

public class CollectorExample1 {
    public static void main(String[] args) {


        List<Person> persons = Arrays.asList(new Person("Max", 18), new Person("Peter", 23), new Person("Pamela", 23), new Person("David", 12));
        System.out.println("List items");
        System.out.println(persons);
    }
}