package concepts.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.*;


public class StreamExample3 {
    public static void main(String[] args) {
        List<Integer> mylist = new ArrayList<Integer>();
        mylist.add(1);
        mylist.add(2);
        mylist.add(3);

        System.out.println("Array initial elements" + mylist);
        System.out.println("Array after 1st modification");
        mylist.stream().map(n->2*n+1).forEach(System.out::println);
        System.out.println("Array after 2nd modification average");
        //mylist.stream().map(n->2*n+1).average().ifpresent(System.out::println);

    }
}