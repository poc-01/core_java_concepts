package concepts.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class StreamExample2 {
    public static void main(String[] args) {
        List<String> mylist = new ArrayList<String>();
        mylist.add("b1");
        mylist.add("b2");
        mylist.add("a3");
        mylist.add("a2");
        mylist.add("a1");
        mylist.add("c2");
        mylist.add("c3");
        System.out.println("Array initial elements"+mylist);
        System.out.println("Array after 1st modification");
        mylist.stream().filter(s -> s.startsWith("a")).map(String::toUpperCase).sorted().forEach(System.out::println);
        System.out.println("Array after 2nd modification");
        mylist.stream().findFirst().ifPresent(System.out::println);
        System.out.println("Intstream");
        IntStream.range(1,5).forEach(System.out::println);
    }
}