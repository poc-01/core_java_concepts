package concepts.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample6 {
    public static void main(String[] args) {
        Stream.of("a1","b4","t4","v4","y6","r5")
                .map(s -> {
                    System.out.println("map  " +s);
                    return s.toUpperCase();
                })
        .anyMatch(s->{
                    System.out.println("match  " +s);
                    return s.startsWith("V");
        });


    }
}