package concepts.interfac;

public class AssembledPc implements Intel,LG,Logitech{
    @Override
    public void processor() {
        System.out.println("inlet processor-4ghz");
    }

    @Override
    public void monitor() {
        System.out.println("lg 14 inch monitor");
    }

    @Override
    public void input() {
        System.out.println("logitech wireless mouse keyboard");
    }
    public static void main(String[] args)
    {
        AssembledPc assembledPc=new AssembledPc();
        assembledPc.processor();
        assembledPc.monitor();
        assembledPc.input();

        Intel intel=new AssembledPc();
        intel.processor();
       // intel.monitor();
        LG lg =new AssembledPc();
        lg.monitor();


    }
}
