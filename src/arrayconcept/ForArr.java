package arrayconcept;

public class ForArr {

    public static void main(String[] args){
        int arr[] ={10,20,30,40,50,60};
        for (int i=arr.length-1;i>=0;i--){
            System.out.println(arr[i]);
        }
        System.out.println("_____________");
        for (int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
        System.out.println("_____________");
        for (int i=0;i<arr.length;i=i+2){
            System.out.println(arr[i]);
        }
    }
}
