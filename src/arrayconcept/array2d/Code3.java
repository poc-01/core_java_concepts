package arrayconcept.array2d;
import java.util.Scanner;
public class Code3 {
    public static void main(String[] args) {
        System.out.println("Enter row & Column size");
        Scanner ob = new Scanner(System.in);
        int m = ob.nextInt();
        int n = ob.nextInt();
        int arr[][] = new int[m][n];
        System.out.println("Enter" + (m * n) + "elements");
        for (int i = 0; i <= (m - 1); i++)
            for (int j = 0; j <= (n - 1); j++)
                arr[i][j] = ob.nextInt();
        for (int i = 0; i <= (m - 1); i++) {
            for (int j = 0; j <= (n - 1); j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
