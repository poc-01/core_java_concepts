package arrayconcept;

public class Arr3 {
    static int x[];
    public static void main(String[] args)
    {

        System.out.println(Arr3.x);//default memory
        Arr3.x = new int[5];
       System.out.println(Arr3.x);//memory after initializing
    }
}
