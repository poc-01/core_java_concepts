package cmdpattern;

public class CmdDemo {

    public static void main(String[] args)
    {
        Stock laptopstock = new Stock();
        BuyStock buyStock = new BuyStock(laptopstock);
        SellStock sellStock = new SellStock(laptopstock);
        Broker broker = new Broker();
        broker.takeOrder(buyStock);
        broker.takeOrder(sellStock);
        broker.placeOrder();
    }
}
