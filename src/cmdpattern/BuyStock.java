package cmdpattern;

public class BuyStock implements Order{
    Stock laptopstock = new Stock();
    public BuyStock(Stock laptopstock){
        this.laptopstock=laptopstock;
    }
    public void execute(){
        laptopstock.buy();
    }
}
