package cmdpattern;

public interface Order {
    void execute();
}
