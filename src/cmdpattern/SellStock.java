package cmdpattern;

public class SellStock implements Order{
    Stock laptopstock = new Stock();
    public SellStock(Stock laptopstock){
        this.laptopstock=laptopstock;
    }
    public void execute(){
        laptopstock.sell();
    }
}
