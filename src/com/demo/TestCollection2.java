package com.demo;
import java.util.*;
import java.util.HashSet;
import java.util.Iterator;

public class TestCollection2 {

    public static void main(String args[]) {

        HashSet<String> set = new HashSet<>();
        set.add("ravi");
        set.add("vela");
        set.add("mano");
        set.add("ravi");
        set.add("kala");
        set.add("nelson");
        Iterator<String> itr = set.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }

}


