package oops.pojoconcept;

public class Pojo{
    private int a,b;
    public Pojo()
    {
        //default;
    }
    public Pojo(int x,int y)
    {
        this.a=x;
        this.b=y;
    }
    public int getA(){
        return this.a;
    }
    public int getB(){
        return this.b;
    }
    public void setA(int m){
        this.a=m;
    }
    public void setB(int n)
    {
        this.b=n;
    }
}
