package oops.pojoconcept;

public class Access {
    public static void main(String[] args)
    {
        Pojo ob=new Pojo();
        ob.setA(1000);
        ob.setB(2000);
        int e = ob.getA();
        int f = ob.getB();
        System.out.println("e value is : "+e);
        System.out.println("f value is : "+f);
    }
}
