package concept.exception;

public class MyException extends Exception{

    MyException(String name){
        super(name);
    }

    public static void main(String[] args) {
        try {
            MyException obj = new MyException("user exception");
            throw obj;
        }
        catch (MyException me){
            System.out.println("messgae "+me.getMessage());
        }
    }
}
