package concept.exception;

import java.util.Scanner;

public class ExcDemo {

    public static void main(String[] args){
        int a,b,c;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter 2 int");
        a=scanner.nextInt();
        b=scanner.nextInt();
        try {
            c = a / b;
            System.out.println("Result " + c);
        }

        catch (ArithmeticException AE){
            System.out.println("Denominator should not zero");
        }

        finally {
            System.out.println("finally block");
        }
    }
}
