package concept.annotation;

public class B extends A{
    @Override
    public void showTimeTv()
    {
        System.out.println("in b");
    }



    public static void main(String[] args){
        A a =new A();
        a.strike();  // dont use warning with strike symbol
        B b =new B();
        b.showTimeTv();
       // A aa = new B();
      //  aa.showTimeTv();

    }
}
