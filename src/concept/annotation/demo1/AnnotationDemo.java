package concept.annotation.demo1;

import java.lang.annotation.Annotation;

public class AnnotationDemo {


    public static void main(String[] args)
    {
        Nokia nokia = new Nokia(5,"f series");
        System.out.println(nokia.model);
        System.out.println(nokia.size);

        Class c = nokia.getClass();
        Annotation an = c.getAnnotation(smartphone.class);
        smartphone s = (smartphone) an;
        System.out.println(s.os());
        System.out.println(s.version());
    }
}
