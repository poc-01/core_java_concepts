package concept.objcast;

public class Postman {
    public static void main(String[] args){
        Owner owner=new Owner();
        owner.mail();
        Owner T1=new Tenant1();
        T1.mail();
        Owner T2=new Tenant2();
        T2.mail();
        Owner T3=new Tenant3();
        T3.mail();
    }
}
