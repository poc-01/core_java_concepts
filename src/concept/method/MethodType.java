package concept.method;

public class MethodType {
    public static void main(String[] args)
    {

        System.out.println();
       Test();  //Method is not taking input and not giving output:

    Test1(10);  //Method is taking input and no output:
        int x=Test2(10); //Method is taking input and returning output:
        System.out.println("x value is "+x);

    }
    static void Test()
    {
        System.out.println("method with No ip No op");
    }
    static void Test1(int a)
    {
        System.out.println("method with ip - No op");
        System.out.println("a value is "+a);
    }
    static int Test2(int a)
    {
        System.out.println("method with ip & op");
       int c=a+10;
       return c;
    }
}
