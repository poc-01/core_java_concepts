package concept.members;

public class StaticBlock {
    static
    {
        System.out.println("Static - 1");
    }
    public static void main(String[] args)
    {
      System.out.println("Main method");

    }
    static
    {
        System.out.println("Static - 2");
    }
}
