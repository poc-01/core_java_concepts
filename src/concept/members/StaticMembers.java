package concept.members;

public class StaticMembers {
    static int x=10;
    static int a=10;
    static int z;
    public static String a (){
        return "i";
    }
    public static void main(String[] args)
    {
        int x=20;
        //int y;
        //System.out.println(" Y Value is "+y); // Error : 'y' is not initialized. Since local variable
        System.out.println(" z static var Value is "+z); // •	Static variables automatically initializes with default values
        System.out.println("Value is x - local variable "+x);
        System.out.println("a Value is "+a);  /*When we access a variable directly with in block, first it is looking for local variable.
        If the local variable is not present, it access static variable.*/
        System.out.println("Value of x - static variable "+StaticMembers.x);
        System.out.println("hello");
        System.out.println(a());
        a();
    }
//    a();
//    System.out.println(a());
}
//main fin fkhdsjkfhsdk