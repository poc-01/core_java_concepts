package concept.thread;

import java.util.Scanner;

public class JoinDemo extends Thread{
    static int n, sum=0;
    public static void main(String [] args) throws InterruptedException
    {
        JoinDemo obj =new JoinDemo();
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter n value: ");
        JoinDemo.n=scanner.nextInt();
        obj.start();
        obj.join();
        System.out.println("sum is : "+JoinDemo.sum);
    }
    public void run()
    {
        for (int i=1;i<=JoinDemo.n;i++)
        {
            JoinDemo.sum=JoinDemo.sum+i;
            try {
                Thread.sleep(500);
            }
            catch (InterruptedException ie){}
        }
        System.out.println("cal end");
    }
}
