package concept.thread.multithread;

public class MultiThread {
    public static void main(String[] args) {
        //MultiThreadThing multiThreadThing = new MultiThreadThing();
        //MultiThreadThing multiThreadThing1 = new MultiThreadThing();
        //multiThreadThing.start();
        //multiThreadThing1.start();

        for (int i = 1; i <= 5; i++) {
            MultiThreadThing multiThreadThing = new MultiThreadThing(i);
            Thread mythread = new Thread(multiThreadThing);
            mythread.start();
            try {
                mythread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
