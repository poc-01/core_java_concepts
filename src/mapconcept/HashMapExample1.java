package mapconcept;

import java.util.*;


public class HashMapExample1 {

    public static void main(String args[]) {
        HashMap<Integer,String> map = new HashMap<Integer,String>();
        map.put(1,"Mango");
        map.put(2,"Apple");
        map.put(3,"Banana");
        map.put(4,"Grapes");
        map.put(3,"bun");
        map.put(0,"jam");
        map.put(null,"jam butter");
        map.put(null,"jam butter star");
        System.out.println("Iterating hash map");
        for(Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey()+" "+m.getValue());
        }

    }
}
