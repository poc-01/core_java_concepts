package mapconcept;
import java.util.*;
import java.util.HashMap;

public class HashExample4 {

      public static void main(String[] args) {

        HashMap<Integer, String> hm = new HashMap<Integer, String>();
        hm.put(100, "Arul");
        hm.put(101, "bala");
        hm.put(102, "kani");
        System.out.println("listing initial" + hm);
        hm.replace(100,"anand");
          System.out.println("updated initial" + hm);
          hm.replaceAll((k,v)->"xxx");
          System.out.println("updated  final initial" + hm);
    }
}