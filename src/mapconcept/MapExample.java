package mapconcept;

import java.util.HashMap;
import java.util.Map;


public class MapExample
{
    public static void main(String[] args)
    {
        Book b1 = new Book(101,"electronics basics","godsey","Dija",2);
        Book b2 = new Book(102,"everthing theory","Einstein","pal",5);
        Book b3 = new Book(103,"physics basics","Tamas","keen",3);
        Map<Integer,Book> map1 = new  HashMap<Integer,Book>();
        map1.put(1,b1);
        map1.put(2,b2);
        map1.put(3,b3);

        System.out.println("List of books");
        for(Map.Entry<Integer,Book> entry : map1.entrySet())
        {
            int k = entry.getKey();
            Book b =entry.getValue();
            System.out.println(k+"details");
            System.out.println(b.id+" "+b.name+" "+b.author+" "+b.publisher+" "+b.qty);
        }

    }
}