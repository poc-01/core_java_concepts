package mapconcept;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class TreeMapExample1 {


    public static void main(String args[]) {
        Map<Integer,String> map = new TreeMap<Integer,String>();
        map.put(1,"Mango");
             map.put(3,"Banana");
        map.put(4,"Grapes");

        map.put(0,"jam");
        map.put(2,"Apple");

        System.out.println("Iterating tree map");
        for(Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey()+" "+m.getValue());
        }

    }
}
