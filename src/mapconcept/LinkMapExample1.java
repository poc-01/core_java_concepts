package mapconcept;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LinkMapExample1 {


    public static void main(String args[]) {
        LinkedHashMap<Integer, String> map = new LinkedHashMap<Integer, String>();
        map.put(1, "Mango");
        map.put(2, "Apple");
        map.put(3, "Banana");
        map.put(4, "Grapes");
        map.put(3, "bun");
        map.put(0, "jam");

        System.out.println("Iterating hash map");
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        List a = map.entrySet()
                .stream()
                .filter(d -> d.getKey() > 5)
                .map(Map.Entry::getValue) // entry -> entry.getValue()
                .collect(Collectors.toList());
        System.out.println(a);
        List a1 = new ArrayList();
        for (Map.Entry m : map.entrySet()) {
            a1.add(m.getValue());
        }
        System.out.println(a1);


    }
}

/*
stream()
    filter()
    map(), flatMap()
    forEach()

Collectors.toList()
Collectors.toMap()
Collectors.toSet()
 */
