package mapconcept;
import java.util.*;
import java.util.HashMap;

public class HashExample3 {


    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<Integer, String>();
        hm.put(100, "Arul");
        hm.put(101, "bala");
        hm.put(102, "kani");
        System.out.println("listing initial" + hm);
        hm.remove(100);
        System.out.println("first update list" + hm);
        hm.remove(102,"kani");
        System.out.println("Second update list" + hm);
    }
}