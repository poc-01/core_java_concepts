package mapconcept;
import java.util.*;
public class HashExample2 {
    public static void main(String[] args){
        HashMap<Integer,String> hm = new HashMap<Integer,String>();
        hm.put(100,"Arul");
        hm.put(101,"bala");
        hm.put(102,"kani");
        System.out.println("listing hashmap");
        for(Map.Entry m:hm.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
            hm.putIfAbsent(103,"mani");
            System.out.println("listing hashmap after addeing element");
            for(Map.Entry m:hm.entrySet()){
                System.out.println(m.getKey()+" " +m.getValue());

        }

        HashMap<Integer,String> map = new HashMap<Integer,String>();
        map.put(104,"siva");
        map.putAll(hm);
        System.out.println("listing hashmap after addeing all element");
        for(Map.Entry m:map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }

    }
}
