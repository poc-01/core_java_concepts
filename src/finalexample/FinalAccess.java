package finalexample;

public class FinalAccess {
    public static void main(String[] args) {
        FinalClass finalClass = new FinalClass();
        finalClass.finMet();
        double x = finalClass.pi;
        System.out.println(x);

    }
}
