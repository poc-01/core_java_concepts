package jdbcdemo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class StatementObjectBatchProcessing {
    public static void main(String[] args) throws ClassNotFoundException, SQLException
    {
        //connection test
        //register jdbc driver
        Class.forName("org.h2.Driver");
        Connection con = DriverManager.getConnection("jdbc:h2:file:E:/Data/softwares/h2/db/training");
        if(con==null)
        {
            System.out.println("conncection not established");

        }
        else {
            System.out.println("conncection established");
        }

    }
}
