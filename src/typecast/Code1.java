package typecast;

public class Code1 {
    public static void main(String[] args)
    {
        byte x=10;
        int y=x;
        System.out.println(x);

        int a=20;
        byte b=(byte)a;
        System.out.println(b);

        int c=131;
        byte d=(byte)c;
        System.out.println(d);//circle diagram char range -128 to 127
    }
}
