package exercise.bank;

import java.util.Scanner;

public class Bank {
    public static void main(String[] args){
        System.out.println("Enter initial amount: ");
        Scanner scanner=new Scanner(System.in);
       int amount= scanner.nextInt();
        Account acc=new Account(amount);
        System.out.println("current balance: "+acc.getBalance());
        System.out.println("Enter withdraw amount: ");

        int drawamount= scanner.nextInt();

        try {
            acc.withdraw(drawamount);
        }
        catch (InsufficientBal insufficientBal)
        {
           System.out.println("message: "+insufficientBal.getMessage());
        }

    }
}
