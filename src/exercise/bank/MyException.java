package exercise.bank;

class InsufficientBal extends Exception{
    InsufficientBal(String name){
        super(name);
    }
}
