package boxing.unboxing;

public class Code3 {
    public static void main(String[] args){

        int a=10;
        System.out.println("a value : " + a);
        String b= Integer.toString(a);
        System.out.println("b value : " + b);
        Integer c= Integer.valueOf(b);
        System.out.println("c value : " + c);
        int d = c.intValue();
        System.out.println("d value : " + d);
        Integer e= Integer.valueOf(d);
        System.out.println("e value : " + e);
        String f=e.toString();
        System.out.println("f value : " + f);
        int g=Integer.valueOf(f);
        System.out.println("g value : " + g);
    }
}
