package boxing.unboxing;

public class Code2 {
    public static void main(String[] args)
    {
        String a="10";
        int x=Integer.valueOf(a);
        System.out.println(x);
        Integer y=Integer.valueOf(x);
        System.out.println(y);
        int m=y.intValue();
        System.out.println(m);
        String n = Integer.toString(m);
        System.out.println(n);
    }
}
