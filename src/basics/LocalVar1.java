package basics;

public class LocalVar1 {
    LocalVar1 var1=new LocalVar1();
    LocalVar1(){
        this.var1=var1;
    }
    public static void main(String[] args){
        int a=10;
        System.out.println("main var value" + a);
    }
    static {
        int a=20;
        System.out.println("static block var value "+a);
    }
    {
        System.out.println("empty block");
    }
}
