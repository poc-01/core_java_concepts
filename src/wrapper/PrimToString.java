package wrapper;

public class PrimToString {


    public static void main(String[] args){
        PrimToObj1 obj = new PrimToObj1();
        int a=10;
        //float a =10.0f;
        System.out.println("prim value "+a);
        System.out.println("prim value "+(a+10));
        //Float b = Float.valueOf(a);
        //Integer b = Integer.valueOf(a);
        Integer b = Integer.valueOf(a);
        System.out.println("obj value b "+b);
        System.out.println("obj value b "+(b+10));
        String c = b.toString();
        System.out.println("string value c "+c);
       System.out.println("string value c "+(c+10));


    }

}
