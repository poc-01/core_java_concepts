package demo.threadEx1;

class OddEvenNum{
    synchronized void even(Number num) throws InterruptedException
    {
        int m=0;
        while( m<10) {
            for (int i = 0; i < num.a.length; i++) {
                if ((num.a[i] % 2) == 0) {

                    System.out.println(num.a[i]);

                } else {
                    wait();
                }

            }
            m++;
            notifyAll();
        }


    }

    synchronized void odd(Number num) throws InterruptedException
    {
        int n=0;
        while (n<10) {
            for (int i = 0; i < num.a.length; i++) {
                if ((num.a[i] % 2) != 0) {

                    System.out.println(num.a[i]);
                } else {
                    wait();
                }

            }
n++;
            notifyAll();
        }

    }
}

