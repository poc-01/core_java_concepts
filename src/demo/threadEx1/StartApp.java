package demo.threadEx1;

public class StartApp {
    public static void main(String[] args) {
        Number number = new Number();
        OddThread ot = new OddThread(number);
        EvenThread et = new EvenThread(number);
        ot.start();
        et.start();
        System.out.println();


    }
}
