package demo.atm;

public class InsufficientBal extends Exception{
    InsufficientBal(String name){
        super(name);
    }
}
