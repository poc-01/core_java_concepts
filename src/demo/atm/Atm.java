package demo.atm;

public class Atm {

    public static void main(String[] args)throws InterruptedException{
        Account account = new Account(10000);
        System.out.println("Starting balance: "+account.getBalance());
        Threads t1=new Threads(account,2000);
        Threads t2=new Threads(account,4000);
t1.start();
t2.start();
t1.join();
t2.join();
        System.out.println("final balance: "+account.getBalance());
    }
}
