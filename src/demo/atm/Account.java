package demo.atm;


public class Account {


    private int balance;
    Account(int amt)
    {
        this.balance=amt;
    }
    public int getBalance() {
        return balance;
    }
    synchronized void withdraw(int amt)throws InsufficientBal
    {
        System.out.println("withdraw amt: " + amt);
        if(this.balance>=amt) {
            this.balance = this.balance - amt;

            System.out.println( "Withdraw "+amt+"after balance: "+this.balance);
        }
        else
        {
            InsufficientBal obj = new InsufficientBal("in sufficient balance");
            throw obj;
        }
    }
}
