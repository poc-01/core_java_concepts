package demo.atm;

import kotlin.jvm.Synchronized;

public class Threads extends Thread{
    Account account;
    int amt;
    Threads(Account account,int amt)
    {
        this.account=account;
        this.amt=amt;
    }

    public void run()
    {
            try{
                account.withdraw(amt);
            }
            catch (InsufficientBal ib)
            {
                System.out.println("error: "+ib.getMessage());
            }

    }
}
