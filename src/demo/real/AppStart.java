package demo.real;

public class AppStart {
    public static void main(String[] args) {
        Bus bus = new Bus();
        Driver driver = new Driver(bus);
        driver.control();
    }
}
