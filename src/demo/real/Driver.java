package demo.real;

public class Driver {
    Bus controller;
    Driver(Bus bus){
        this.controller=bus;
    }

    void control(){
        this.controller.accelarator();
        this.controller.staring();
        this.controller.horn();
        this.controller.breaking();
    }
}
